import img from '../../../assets/images/delete.png'

const ModalImage = () =>{
    return(
        <img className='modal-img' src={img} alt="wrong path" />
    )
}

export default ModalImage;