import ModalBody from "./ModalBody";
import ModalClose from "./ModalClose";
import ModalFooter from "./ModalFooter";
import ModalHeader from "./ModalHeader";
import ModalWrapper from "./ModalWrapper";

const Modal = (props) => {
  const {
    headerCont,
    bodyCont,
    firstButtonText,
    addToCart,
    className,
    isModalOpen,
    onCloseClick
  } = props;
  const modalStyle = {
    display: isModalOpen ? 'flex' : 'none'
};

    return (
        <div className={`modal ${className}`} style={modalStyle}>
          <ModalWrapper>
            <ModalHeader>
              {headerCont}
            </ModalHeader>
            <ModalClose onClick={onCloseClick} />
            <ModalBody>{bodyCont}</ModalBody>
            <ModalFooter
              firstText={firstButtonText}
              firstClick={addToCart}
            ></ModalFooter>
          </ModalWrapper>
        </div>
      );
  }
    

export default Modal;
