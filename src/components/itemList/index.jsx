import Item from "../Item"
import propTypes from 'prop-types'

const ItemList = ({ items = [], FavouriteFn = () => {}, openModal = () => {}, addToCart = () => {} }) => {
    return (
      <div className="itemContainer">
        {items &&
          items.map((item) => (
            <Item
              key={item.articul}
              articul={item.articul}
              item={item}
              FavouriteFn={FavouriteFn}
              isFavourite={item.isFavourite}
              openModal={() => openModal(item)} // Pass item details to openModal
              addToCart={addToCart}
            ></Item>
          ))}
      </div>
    );
  };

ItemList.propTypes = {
    items: propTypes.array,
    FavouriteFn: propTypes.func,
    isFavourite: propTypes.bool
}

export default ItemList;