import propTypes from 'prop-types'
import StarSvg from '../svg/StarSvg';
import classNames from 'classnames';
import Button from '../Button';

const Item =({item={}, articul, FavouriteFn=() =>{}, isFavourite = false, openModal=() =>{}, addToCart=() =>{}}) =>{
    return(
        <div className='item'>
          <img className='item-img' src={item.img} alt="" />
            <h1 className='item-name'>{item.name}</h1>
            <h2 className='item-price'>{item.price}</h2>
            <div className='item-btns'>
            <Button className='add-to-cart' onClick={openModal}>Add to cart</Button>
            <div className={classNames('add-to-favourites', {'favourite-active': isFavourite})} onClick={() => FavouriteFn(articul)}>
            <StarSvg></StarSvg>
            </div>
            </div>

        </div>

    )
}

Item.propTypes ={
    item: propTypes.object,
    FavouriteFn: propTypes.func,
    isFavourite: propTypes.bool,
    openModal: propTypes.func
}

export default Item;