import Cart from "../svg/CartSvg";
import StarSvg from "../svg/StarSvg";

const Header = ({cartNumber, favNumber}) => {
  return (
    <header>
      <div className="fav">
        <StarSvg></StarSvg>
      </div>
      <p>{favNumber}</p>

      <div className="cart">
    <Cart></Cart>
      </div>
      <p>{cartNumber}</p>
    </header>
  );
};

export default Header;
