import { useState, useEffect } from "react";
import { useImmer } from "use-immer";
import axios from "axios";
import ItemList from "./components/itemList";
import "./App.scss";
import Header from "./components/Header";
import Modal from "./components/Modal";

function App() {
  const URL = "./arr.JSON";
  const [items, setItems] = useImmer([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);

  useEffect(function () {
    async function fetchItems() {
      const { data } = await axios.get(URL);
      setItems(data);
    }
    fetchItems();
  }, []);

  const openModal = (item) => {
    setSelectedItem(item);
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const addToCart = (articul) =>{
    setItems((draft) =>{
      const item = draft.find((item) => articul === item.articul)
      if (item) {
        item.isInCart = true; 
      }
    })

    closeModal()
  }

  const CartCounter = () => {
    const cart = items.filter((item) => item.isInCart === true);
    const cartNumber = cart.length;
    return cartNumber;
  };


  const FavCounter = () => {
    const favourite = items.filter((item) => item.isFavourite === true);
    const favNumber = favourite.length;
    return favNumber;
  };

  const FavouriteFn = (articul) => {
    setItems((draft) => {
      const item = draft.find((item) => articul === item.articul);
      item.isFavourite = !item.isFavourite;

    });
  };
  
  const cartNumber = CartCounter()
  const favNumber = FavCounter();

  return (
    <>
      <Header favNumber={favNumber} cartNumber={cartNumber}></Header>
      <ItemList
        items={items}
        FavouriteFn={FavouriteFn}
        openModal={openModal}
        addToCart={addToCart}
      ></ItemList>
      {selectedItem && (
        <Modal
          headerCont="Add to cart?"
          bodyCont={`Add ${selectedItem.name} to cart?`}
          firstButtonText="Add to favorites"
          addToCart={() => addToCart(selectedItem.articul)}
          className="fav-modal"
          isModalOpen={isModalOpen}
          onCloseClick={closeModal}
        ></Modal>
      )}
    </>
  );
}
export default App;
